package com.ruoyi.common.utils.ip;

/**
 * 获取地址接口
 * @author bigroc
 */
public interface IAddressIP
{
    String getRealAddressByIP(String ip);
}
